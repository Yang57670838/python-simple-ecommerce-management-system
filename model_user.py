class User:
    def __init__(self, user_id='u_1234567890', user_name='username', user_password='password1', user_register_time='00-00-0000_00:00:00', user_role='customer'):
        self.user_id = user_id
        self.user_name = user_name
        self.user_password = user_password
        self.user_register_time = user_register_time
        self.user_role = user_role

    # the requirement PDF showes format break into different lines with \n, so i'm just following
    def __str__(self):
        return "{'user_id':'" + self.user_id + "', 'user_name':'" + self.user_name + "',\n" + "'user_password':'" + self.user_password + "', 'user_register_time':'" + self.user_register_time + "',\n" + "'user_role':'" + self.user_role + "'}"
