def read_whole_txt_file(fpath):
    with open(fpath) as f:
        return f.read()   

def write_new_line_to_txt(fpath, str):
    with open(fpath, 'a', encoding='utf-8') as f:
        f.write(str)
        f.write('\n')