import time

# 00-00-0000_00:00:00, format: DD-MM-YYYY_HH:MM:SS
def format_curent_datetime():
    datetime_tuple = time.localtime()
    return time.strftime('%d-%m-%Y_%H:%M:%S', datetime_tuple)
