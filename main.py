from io_interface import IOInterface
from operation_user import UserOperation
from operation_customer import CustomerOperation
from operation_product import ProductOperation
from operation_order import OrderOperation
from operation_admin import AdminOperation


def login_control():
    io_control = IOInterface()
    user_operation = UserOperation()
    username_password_inputs = io_control.get_user_input(
        'Please enter your login user name and password', '2')
    # will let user_operation to validate input username and password
    login_result = user_operation.login(
        username_password_inputs[0], username_password_inputs[1])
    if login_result is None:
        # if login fail, will go back to main menu, or it will stuck in the login input forever..
        io_control.print_error_message(
            'UserOperation.login', 'username or password incorrect')
        return None
    else:
        return login_result


def customer_control(login_object):
    io_control = IOInterface()
    customer_operation = CustomerOperation()
    product_operation = ProductOperation()
    order_operation = OrderOperation()
    is_customer_control_continue = True
    while is_customer_control_continue:
        io_control.customer_menu()
        # ask for customer menu select
        customer_menu_select = io_control.get_user_input(
            'Please enter your selection', '2')
        if customer_menu_select[0].isdigit():
            if customer_menu_select[0] == '1':
                # Show profile
                io_control.print_object(login_object)
            elif customer_menu_select[0] == '2':
                # Update profile
                customer_update_profile_input = io_control.get_user_input(
                    'Which attribute do you want to update? what is the new value?', '2')
                # let update_profile method itself control the input validation
                update_result = customer_operation.update_profile(
                    customer_update_profile_input[0], customer_update_profile_input[1], login_object)
                if update_result:
                    # update_profile success
                    io_control.print_message('Updating successfully!!')
                else:
                    # update_profile failed
                    io_control.print_error_message(
                        'CustomerOperation.update_profile', 'updating value or attribute is not valid!!')
            elif customer_menu_select[0] == '3':
                # Show products (user input could be '3 keyword' or '3')
                if customer_menu_select[1] == '':
                    # show by pagination, start from page 1
                    product_list_result = product_operation.get_product_list(
                        '1')
                    io_control.show_list(
                        'customer', 'Product', product_list_result)
                    continue_show_products_in_pagination = True
                    while continue_show_products_in_pagination:
                        continue_show_products_pagination_input = io_control.get_user_input(
                            'Do you want to continue see more pages? (Yes/No)', '1')
                        if continue_show_products_pagination_input[0] == 'Yes':
                            next_products_page_input = io_control.get_user_input(
                                'Which page do you want to see next?', '1')
                            if next_products_page_input[0].isdigit():
                                product_list_result = product_operation.get_product_list(
                                    next_products_page_input[0])
                                io_control.show_list(
                                    'customer', 'Product', product_list_result)
                            else:
                                io_control.print_message(
                                    'Please enter digits page number only..')
                        elif continue_show_products_pagination_input[0] == 'No':
                            continue_show_products_in_pagination = False
                else:
                    # search by keyword
                    keyword = customer_menu_select[1]
                    search_result = product_operation.get_product_list_by_keyword(
                        keyword)
                    if len(search_result) == 0:
                        # found no product matches
                        io_control.print_error_message(
                            'ProductOperation.get_product_list_by_keyword', 'Search products by keyword failed, could not find by this keyword..!')
                    else:
                        for one_search_result in search_result:
                            io_control.print_object(one_search_result)
            elif customer_menu_select[0] == '4':
                # Show history orders
                # show by pagination, start from page 1
                order_list_by_customer_result = order_operation.get_order_list(
                    login_object.user_id, '1')
                io_control.show_list('customer', 'Order',
                                     order_list_by_customer_result)
                continue_show_orders_in_pagination = True
                while continue_show_orders_in_pagination:
                    continue_show_orders_pagination_input = io_control.get_user_input(
                        'Do you want to continue see more pages? (Yes/No)', '1')
                    if continue_show_orders_pagination_input[0] == 'Yes':
                        next_orders_page_input = io_control.get_user_input(
                            'Which page do you want to see next?', '1')
                        if next_orders_page_input[0].isdigit():
                            order_list_by_customer_result = order_operation.get_order_list(
                                login_object.user_id, next_orders_page_input[0])
                            io_control.show_list(
                                'customer', 'Order', order_list_by_customer_result)
                        else:
                            io_control.print_message(
                                'Please enter digits page number only..')
                    elif continue_show_orders_pagination_input[0] == 'No':
                        continue_show_orders_in_pagination = False
            elif customer_menu_select[0] == '5':
                # Generate all consumption figures
                # assume only generate_single_customer_consumption_figure() and generate_all_customers_consumption_figure()
                # put inside a try block
                try:
                    io_control.print_message(
                        'Starting generate single customer consumption figures...')
                    io_control.print_message(
                        'It will be a little bit slow, please wait')
                    order_operation.generate_single_customer_consumption_figure(
                        login_object.user_id)
                except:
                    io_control.print_error_message(
                        'OrderOperation.generate_single_customer_consumption_figure', 'generate fingure failed, please try again later..')
                else:
                    try:
                        io_control.print_message(
                            'Starting generate all customers consumption figures...')
                        io_control.print_message(
                            'It will be super slow, please wait couple minutes..')
                        order_operation.generate_all_customers_consumption_figure()
                    except:
                        io_control.print_error_message(
                            'OrderOperation.generate_all_customers_consumption_figure', 'generate fingure failed, please try again later..')
                    else:
                        io_control.print_message(
                            'Successfully generate all consumption figures!!')
                        io_control.print_message(
                            'Please find them under data/figure..')
            elif customer_menu_select[0] == '6':
                # Logout
                io_control.print_message('You will be logout now.')
                is_customer_control_continue = False
            else:
                io_control.print_message('There are only 6 options available')
        else:
            io_control.print_message('Only digit inputs are allowed')


def admin_control():
    io_control = IOInterface()
    customer_operation = CustomerOperation()
    product_operation = ProductOperation()
    order_operation = OrderOperation()
    is_admin_control_continue = True
    while is_admin_control_continue:
        io_control.admin_menu()
        # ask for admin menu select
        admin_menu_select = io_control.get_user_input(
            'Please enter your selection', '1')
        if admin_menu_select[0].isdigit():
            if admin_menu_select[0] == '1':
                # Show products
                # show by pagination, start from page 1
                product_list_result = product_operation.get_product_list('1')
                io_control.show_list('admin', 'Product', product_list_result)
                continue_show_products_in_pagination = True
                while continue_show_products_in_pagination:
                    continue_show_products_pagination_input = io_control.get_user_input(
                        'Do you want to continue see more pages? (Yes/No)', '1')
                    if continue_show_products_pagination_input[0] == 'Yes':
                        next_products_page_input = io_control.get_user_input(
                            'Which page do you want to see next?', '1')
                        if next_products_page_input[0].isdigit():
                            product_list_result = product_operation.get_product_list(
                                next_products_page_input[0])
                            io_control.show_list(
                                'admin', 'Product', product_list_result)
                        else:
                            io_control.print_message(
                                'Please enter digits page number only..')
                    elif continue_show_products_pagination_input[0] == 'No':
                        continue_show_products_in_pagination = False
            elif admin_menu_select[0] == '2':
                # Add customers
                add_customer_inputs = io_control.get_user_input(
                    'Please enter user name, password, email, mobile', '4')
                # let register_customer to control the input validation
                register_customer_result = customer_operation.register_customer(
                    add_customer_inputs[0], add_customer_inputs[1], add_customer_inputs[2], add_customer_inputs[3])
                if register_customer_result:
                    io_control.print_message(
                        f'Customer: {add_customer_inputs[0]} register successfully...')
                    io_control.print_message(
                        f'You can login with its password: {add_customer_inputs[1]}')
                else:
                    io_control.print_error_message(
                        'CustomerOperation.register_customer', 'Register customer failed, please make sure all inputs are valid, and username is unique!')
            elif admin_menu_select[0] == '3':
                # Show customers
                # show by pagination, start from page 1
                customer_list_result = customer_operation.get_customer_list(
                    '1')
                io_control.show_list('admin', 'Customer', customer_list_result)
                continue_show_customers_in_pagination = True
                while continue_show_customers_in_pagination:
                    continue_show_customers_pagination_input = io_control.get_user_input(
                        'Do you want to continue see more pages? (Yes/No)', '1')
                    if continue_show_customers_pagination_input[0] == 'Yes':
                        next_customers_page_input = io_control.get_user_input(
                            'Which page do you want to see next?', '1')
                        if next_customers_page_input[0].isdigit():
                            customer_list_result = customer_operation.get_customer_list(
                                next_customers_page_input[0])
                            io_control.show_list(
                                'admin', 'Customer', customer_list_result)
                        else:
                            io_control.print_message(
                                'Please enter digits page number only..')
                    elif continue_show_customers_pagination_input[0] == 'No':
                        continue_show_customers_in_pagination = False
            elif admin_menu_select[0] == '4':
                # Show orders
                # since order operation class only defined get_order_list by customer_id method, as per requirements..
                # so will ask user to enter customer_id first
                customer_id_input = io_control.get_user_input(
                    'What is the customer Id for the orders belong to', '1')
                # show by pagination, start from page 1
                order_list_by_customer_result = order_operation.get_order_list(
                    customer_id_input[0], '1')
                io_control.show_list(
                    'admin', 'Order', order_list_by_customer_result)
                continue_show_orders_in_pagination = True
                while continue_show_orders_in_pagination:
                    continue_show_orders_pagination_input = io_control.get_user_input(
                        'Do you want to continue see more pages? (Yes/No)', '1')
                    if continue_show_orders_pagination_input[0] == 'Yes':
                        next_orders_page_input = io_control.get_user_input(
                            'Which page do you want to see next?', '1')
                        if next_orders_page_input[0].isdigit():
                            order_list_by_customer_result = order_operation.get_order_list(
                                customer_id_input[0], next_orders_page_input[0])
                            io_control.show_list(
                                'admin', 'Order', order_list_by_customer_result)
                        else:
                            io_control.print_message(
                                'Please enter digits page number only..')
                    elif continue_show_orders_pagination_input[0] == 'No':
                        continue_show_orders_in_pagination = False
            elif admin_menu_select[0] == '5':
                # Generate test data
                try:
                    io_control.print_message(
                        'Start to generate 10 customers with randomly 50-200 orders for each...')
                    order_operation.generate_test_order_data()
                except:
                    io_control.print_error_message(
                        'OrderOperation.generate_test_order_data', 'Please try again later.. or maybe delete all current data first..')
                else:
                    io_control.print_message(
                        'All done!! Now you are find new customers inside data/users.txt file, and new orders inside data/orders.txt')
                    io_control.print_message(
                        'all customers have default password as "password1"!!')
            elif admin_menu_select[0] == '6':
                # Generate all statistical figures
                # generate_category_figure, generate_discount_figure, generate_likes_count_figure, generate_discount_likes_count_figure
                # includes: generate_all_customers_consumption_figure, generate_single_customer_consumption_figure, generate_all_top_10_best_sellers_figure
                # put inside multiple try block
                try:
                    io_control.print_message(
                        'Starting generate category figure...')
                    product_operation.generate_category_figure()
                except:
                    io_control.print_error_message(
                        'ProductOperation.generate_category_figure', 'generate category figure failed, please try again later..')
                try:
                    io_control.print_message(
                        'Starting generate discount figure...')
                    product_operation.generate_discount_figure()
                except:
                    io_control.print_error_message(
                        'ProductOperation.generate_discount_figure', 'generate discount figure failed, please try again later..')
                try:
                    io_control.print_message(
                        'Starting generate likes count figure...')
                    product_operation.generate_likes_count_figure()
                except:
                    io_control.print_error_message(
                        'ProductOperation.generate_likes_count_figure', 'generate likes count figure failed, please try again later..')
                try:
                    io_control.print_message(
                        'Starting generate discount likes count figure...')
                    product_operation.generate_discount_likes_count_figure()
                except:
                    io_control.print_error_message('ProductOperation.generate_discount_likes_count_figure',
                                                   'generate discount likes count figure failed, please try again later..')
                try:
                    io_control.print_message(
                        'Starting generate all top 10 best sellers!! figure...')
                    order_operation.generate_all_top_10_best_sellers_figure()
                except:
                    io_control.print_error_message('OrderOperation.generate_all_top_10_best_sellers_figure',
                                                   'generate top 10 best sellers figure failed, please try again later..')
                try:
                    io_control.print_message(
                        'Starting generate all customers comsuption figure...')
                    io_control.print_message(
                        'It will be super slow, please wait couple minutes.....')
                    order_operation.generate_all_customers_consumption_figure()
                except:
                    io_control.print_error_message('OrderOperation.generate_all_customers_consumption_figure',
                                                   'generate all customers comsuption figure failed, please try again later..')
                try:
                    # ask input customer_id for generate_single_customer_consumption_figure
                    customer_id_for_figure = io_control.get_user_input(
                        'Please provide a customer id, make sure it is correct, or the figure will not work', '1')
                    io_control.print_message(
                        'Starting generate comsuption figure for provided single customer...')
                    io_control.print_message(
                        'It will be a bit slow, please wait a little bit .....')
                    order_operation.generate_single_customer_consumption_figure(
                        customer_id_for_figure)
                except:
                    io_control.print_error_message(
                        'OrderOperation.generate_single_customer_consumption_figure', 'Try to provide a correct customer ID please..')
                else:
                    io_control.print_message('done!!')
            elif admin_menu_select[0] == '7':
                # Delete all data
                # delete all customers from users.txt, but will leave admin account there
                try:
                    customer_operation.delete_all_customers()
                except:
                    io_control.print_error_message(
                        'CustomerOperation.delete_all_customers', 'Try again later..')
                else:
                    io_control.print_message('all customers deleted..')
                # delete_all_orders
                try:
                    order_operation.delete_all_orders()
                except:
                    io_control.print_error_message(
                        'OrderOperation.delete_all_orders', 'Try again later..')
                else:
                    io_control.print_message('all orders deleted..')
                # delete_all_products
                try:
                    product_operation.delete_all_products()
                except:
                    io_control.print_error_message(
                        'ProductOperation.delete_all_products', 'Try again later..')
                else:
                    io_control.print_message('all products deleted..')
                    io_control.print_message(
                        'you may want to restart the app now..')
            elif admin_menu_select[0] == '8':
                # Logout
                io_control.print_message('You will be logout now.')
                is_admin_control_continue = False
            else:
                io_control.print_message('There are only 8 options available')
        else:
            io_control.print_message('Only digit inputs are allowed')


def main_menu_control():
    io_control = IOInterface()
    io_control.main_menu()
    # ask for main menu select
    selected_main_menu_option = -1
    is_invalid_main_select = True
    while is_invalid_main_select:
        main_select = io_control.get_user_input(
            'Please enter your selection', '1')
        if main_select[0].isdigit():
            if int(main_select[0]) in range(1, 4):
                selected_main_menu_option = int(main_select[0])
                is_invalid_main_select = False
            else:
                io_control.print_message(
                    'There are only 3 options available here in the main menu')
        else:
            io_control.print_message('Only digit inputs are allowed')
    return selected_main_menu_option


def main():
    # extract products informations from csv files into products.txt when app starts
    io_control = IOInterface()
    io_control.print_message('extracting products.......')
    product_operation = ProductOperation()
    product_operation.extract_products_from_files()
    # register admin
    io_control.print_message('register a new admin account.......')
    admin_operation = AdminOperation()
    admin_operation.register_admin()
    io_control.print_message(
        'done, you can find the new account name in the users.txt file, starts with "admin_"')
    io_control.print_message('default password is: "password1"')
    is_main_menu_continue = True
    while is_main_menu_continue:
        main_menu_select = main_menu_control()
        if main_menu_select == 1:
            # login
            login_result = login_control()
            if login_result is not None:
                if login_result.user_role == 'admin':
                    # go to admin menu
                    admin_control()
                elif login_result.user_role == 'customer':
                    # go to customer men
                    customer_control(login_result)
        elif main_menu_select == 2:
            # register customer account
            customer_operation = CustomerOperation()
            add_customer_inputs = io_control.get_user_input(
                'Please enter customer name, password, email, mobile', '4')
            # let register_customer to control the input validation
            register_customer_result = customer_operation.register_customer(
                add_customer_inputs[0], add_customer_inputs[1], add_customer_inputs[2], add_customer_inputs[3])
            if register_customer_result:
                io_control.print_message(
                    f'Customer: {add_customer_inputs[0]} register successfully...')
                io_control.print_message(
                    f'You can login with its password: {add_customer_inputs[1]}')
            else:
                io_control.print_error_message(
                    'CustomerOperation.register_customer', 'Register customer failed, please make sure all inputs are valid, and username is unique!')
        else:
            # quit
            io_control.print_message('Bye bye..')
            is_main_menu_continue = False


if __name__ == '__main__':
    main()
