from model_user import User


class Customer(User):
    def __init__(self, user_id='u_1234567890', user_name='username', user_password='password1', user_register_time='00-00-0000_00:00:00', user_role='customer', user_email='test@gmail.com', user_mobile='1234567890'):
        super().__init__(user_id, user_name, user_password, user_register_time, user_role)
        self.user_email = user_email
        self.user_mobile = user_mobile

    # the requirement PDF showes format break into different lines with \n, so i'm just following
    def __str__(self):
        return "{'user_id':'" + self.user_id + "', 'user_name':'" + self.user_name + "',\n" + "'user_password':'" + self.user_password + "', 'user_register_time':'" + self.user_register_time + "',\n" + "'user_role':'" + self.user_role + "',\n" + "'user_email':'" + self.user_email + "',\n" + "'user_mobile':'" + self.user_mobile + "'}"
