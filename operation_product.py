import pandas as pd
import os
import math
from model_product import Product
import matplotlib.pyplot as plt


class ProductOperation:
    def extract_products_from_files(self):
        # because this method will be called everytime app starts.. so clean previous data first
        self.delete_all_products()
        current_dir = os.path.dirname(__file__)
        # filter selected columns, also drop duplicated rows, since each Product id must be unique as requirement
        selected_columns = ['id', 'model', 'category', 'name',
                            'current_price', 'raw_price', 'discount', 'likes_count']
        # accessories.csv
        accessories_file_path = os.path.join(
            current_dir, "data/product/accessories.csv")
        accessories_dataframe = pd.read_csv(accessories_file_path)
        filtered_accessories_dataframe = accessories_dataframe[selected_columns].drop_duplicates(
        )
        # bags.csv
        bags_file_path = os.path.join(current_dir, "data/product/bags.csv")
        bags_dataframe = pd.read_csv(bags_file_path)
        filtered_bags_dataframe = bags_dataframe[selected_columns].drop_duplicates(
        )
        # beauty.csv
        beauty_file_path = os.path.join(current_dir, "data/product/beauty.csv")
        beauty_dataframe = pd.read_csv(beauty_file_path)
        filtered_beauty_dataframe = beauty_dataframe[selected_columns].drop_duplicates(
        )
        # house.csv
        house_file_path = os.path.join(current_dir, "data/product/house.csv")
        house_dataframe = pd.read_csv(house_file_path)
        filtered_house_dataframe = house_dataframe[selected_columns].drop_duplicates(
        )
        # jewelry.csv
        jewelry_file_path = os.path.join(
            current_dir, "data/product/jewelry.csv")
        jewelry_dataframe = pd.read_csv(jewelry_file_path)
        filtered_jewelry_dataframe = jewelry_dataframe[selected_columns].drop_duplicates(
        )
        # kids.csv
        kids_file_path = os.path.join(current_dir, "data/product/kids.csv")
        kids_dataframe = pd.read_csv(kids_file_path)
        filtered_kids_dataframe = kids_dataframe[selected_columns].drop_duplicates(
        )
        # men.csv
        men_file_path = os.path.join(current_dir, "data/product/men.csv")
        men_dataframe = pd.read_csv(men_file_path)
        filtered_men_dataframe = men_dataframe[selected_columns].drop_duplicates(
        )
        # shoes.csv
        shoes_file_path = os.path.join(current_dir, "data/product/shoes.csv")
        shoes_dataframe = pd.read_csv(shoes_file_path)
        filtered_shoes_dataframe = shoes_dataframe[selected_columns].drop_duplicates(
        )
        # women.csv
        women_file_path = os.path.join(current_dir, "data/product/women.csv")
        women_dataframe = pd.read_csv(women_file_path)
        filtered_women_dataframe = women_dataframe[selected_columns].drop_duplicates(
        )

        # merging data frame
        merged_data_frame = pd.concat([filtered_accessories_dataframe, filtered_bags_dataframe, filtered_beauty_dataframe, filtered_house_dataframe,
                                      filtered_jewelry_dataframe, filtered_kids_dataframe, filtered_men_dataframe, filtered_shoes_dataframe, filtered_women_dataframe], ignore_index=True)
        # convert into array of dictionary, so we can use loop to write into txt file line by line
        converted_payload = merged_data_frame.to_dict('records')
        # write to txt file
        target_file_path = os.path.join(current_dir, "data/products.txt")
        # write all contents into txt file in a try block, since its writing on same file in a loop
        with open(target_file_path, 'a', encoding='utf-8') as f:
            for record in converted_payload:
                # since pro_name contains single quote or double quote in the middle, replace to avoid issues later
                content = "{'pro_id':'" + str(record.get('id')) + "', 'pro_model':'" + record.get('model') + "', 'pro_category':'" + record.get('category') + "', 'pro_name':'" + record.get('name').replace("'", '') + "', 'pro_current_price':'" + str(
                    record.get('current_price')) + "', 'pro_raw_price':'" + str(record.get('raw_price')) + "', 'pro_discount':'" + str(record.get('discount')) + "', 'pro_likes_count':'" + str(record.get('likes_count')) + "'}"
                f.write(content)
                f.write('\n')

    # since requirement does not mention, will assume string as page_number argument type
    def get_product_list(self, page_number):
        items_per_page = 10
        result_product_list = []
        # read file
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/products.txt")
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        # calculate total pages by round up
        total_page = math.ceil(len(lines) / items_per_page)
        # just in case argument page number is too big for the record available pages
        if int(page_number) > total_page:
            return ([], int(page_number), total_page)
        else:
            # calculate first item index for the page
            first_item_index = (int(page_number) - 1) * items_per_page
            # slice the big list into smaller list just for single page
            # for last page, may not have maximum 10 products, this slice will still work by just return available items
            lines_in_page = lines[first_item_index: first_item_index + items_per_page]
            # create list of Product instance now
            for line in lines_in_page:
                # to convert string of dictionary into dictionary, since no other library is allowed, using eval()
                converted_line_to_dict = eval(line)
                pro_id = converted_line_to_dict.get('pro_id')
                pro_model = converted_line_to_dict.get('pro_model')
                pro_category = converted_line_to_dict.get('pro_category')
                pro_name = converted_line_to_dict.get('pro_name')
                pro_current_price = converted_line_to_dict.get(
                    'pro_current_price')
                pro_raw_price = converted_line_to_dict.get('pro_raw_price')
                pro_discount = converted_line_to_dict.get('pro_discount')
                pro_likes_count = converted_line_to_dict.get(
                    'pro_likes_count')
                new_product = Product(pro_id, pro_model, pro_category, pro_name,
                                      pro_current_price, pro_raw_price, pro_discount, pro_likes_count)
                result_product_list.append(new_product)
            return (result_product_list, int(page_number), total_page)

    # rewrite the file by filter out the particular product record by its product_id
    def delete_product(self, product_id):
        # read file
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/products.txt")
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        deleting_record_index = None
        for index, line in enumerate(lines):
            product_id_in_line = line[11:18]
            if product_id_in_line == product_id:
                deleting_record_index = index
                break
        if deleting_record_index is None:
            return False
        else:
            del lines[deleting_record_index]
            # Write original lines back to file again, but skip the deleting record's 5 lines
            with open(file_path, 'w', encoding='utf-8') as f:
                f.writelines(lines)
        return True

    # This method retrieves all the products whose name contains the keyword (case insensitive).
    def get_product_list_by_keyword(self, keyword):
        result_product_list = []
        # read file
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/products.txt")
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        for line in lines:
            # to convert string of dictionary into dictionary, since no other library is allowed, using eval()
            converted_line_to_dict = eval(line)
            pro_name_in_line = converted_line_to_dict.get('pro_name')
            if keyword.casefold() in pro_name_in_line.casefold():
                pro_id = converted_line_to_dict.get('pro_id')
                pro_model = converted_line_to_dict.get('pro_model')
                pro_category = converted_line_to_dict.get('pro_category')
                pro_current_price = converted_line_to_dict.get(
                    'pro_current_price')
                pro_raw_price = converted_line_to_dict.get('pro_raw_price')
                pro_discount = converted_line_to_dict.get('pro_discount')
                pro_likes_count = converted_line_to_dict.get(
                    'pro_likes_count')
                new_product = Product(pro_id, pro_model, pro_category, pro_name_in_line,
                                      pro_current_price, pro_raw_price, pro_discount, pro_likes_count)
                result_product_list.append(new_product)
        return result_product_list

    def get_product_by_id(self, product_id):
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/products.txt")
        lines = None
        result = None
        with open(file_path) as f:
            lines = f.readlines()
        for line in lines:
            # to convert string of dictionary into dictionary, since no other library is allowed, using eval()
            converted_line_to_dict = eval(line)
            pro_id_in_line = converted_line_to_dict.get('pro_id')
            if pro_id_in_line == product_id:
                pro_name = converted_line_to_dict.get('pro_name')
                pro_model = converted_line_to_dict.get('pro_model')
                pro_category = converted_line_to_dict.get('pro_category')
                pro_current_price = converted_line_to_dict.get(
                    'pro_current_price')
                pro_raw_price = converted_line_to_dict.get('pro_raw_price')
                pro_discount = converted_line_to_dict.get('pro_discount')
                pro_likes_count = converted_line_to_dict.get(
                    'pro_likes_count')
                result = Product(product_id, pro_model, pro_category, pro_name,
                                 pro_current_price, pro_raw_price, pro_discount, pro_likes_count)
                break
        return result

    # This method generates a bar chart that shows the total number of products for each category in descending order.
    # The figure is saved into the data/figure folder.
    def generate_category_figure(self):
        # count for y values
        current_dir = os.path.dirname(__file__)
        products_path = os.path.join(current_dir, "data/products.txt")
        lines = None
        # this dictionary will store each categories how many counts
        categories_count_pointer = {}
        with open(products_path) as f:
            lines = f.readlines()
        for line in lines:
            converted_line_to_dict = eval(line)
            pro_category_in_line = converted_line_to_dict.get('pro_category')
            if pro_category_in_line in categories_count_pointer:
                current_value = categories_count_pointer.get(
                    pro_category_in_line)
                categories_count_pointer[pro_category_in_line] = current_value + 1
            else:
                categories_count_pointer[pro_category_in_line] = 1
        # draw bar chart
        x_categories = list(categories_count_pointer.keys())
        x_categories.sort(reverse=True)
        y_count = []
        for category in x_categories:
            count = categories_count_pointer.get(category)
            y_count.append(count)
        plt.figure(figsize=(10, 6))
        plt.bar(x_categories, y_count, linewidth=1.5)
        plt.xlabel("Categories")
        plt.ylabel("Number of products")
        plt.title("Total number of products for each category in descending order")
        target_path = os.path.join(
            current_dir, "data/figure/generate_category_figure.jpg")
        plt.savefig(target_path)

    def generate_discount_figure(self):
        x = ['<30', '30-60', '>60']
        y = [0, 0, 0]
        # count for y values
        current_dir = os.path.dirname(__file__)
        products_path = os.path.join(current_dir, "data/products.txt")
        lines = None
        with open(products_path) as f:
            lines = f.readlines()
        for line in lines:
            converted_line_to_dict = eval(line)
            pro_discount_in_line = converted_line_to_dict.get('pro_discount')
            if int(pro_discount_in_line) < 30:
                current_value = y[0]
                update_value = current_value + 1
                y[0] = update_value
            elif int(pro_discount_in_line) > 60:
                current_value = y[2]
                update_value = current_value + 1
                y[2] = update_value
            else:
                current_value = y[1]
                update_value = current_value + 1
                y[1] = update_value
        # start draw
        plt.pie(y, labels=x)
        plt.title("Discount Figure")
        target_path = os.path.join(
            current_dir, "data/figure/generate_discount_figure.jpg")
        plt.savefig(target_path)

    def generate_likes_count_figure(self):
        # count for y values
        current_dir = os.path.dirname(__file__)
        products_path = os.path.join(current_dir, "data/products.txt")
        lines = None
        # this dictionary will store each categories how many like counts
        categories_like_count_pointer = {}
        with open(products_path) as f:
            lines = f.readlines()
        for line in lines:
            converted_line_to_dict = eval(line)
            pro_category_in_line = converted_line_to_dict.get('pro_category')
            pro_likes_count_in_line = converted_line_to_dict.get(
                'pro_likes_count')
            if pro_category_in_line in categories_like_count_pointer:
                current_value = categories_like_count_pointer.get(
                    pro_category_in_line)
                categories_like_count_pointer[pro_category_in_line] = current_value + int(
                    pro_likes_count_in_line)
            else:
                categories_like_count_pointer[pro_category_in_line] = int(
                    pro_likes_count_in_line)
        # draw bar chart
        x_categories = list(categories_like_count_pointer.keys())
        x_categories.sort()
        y_count = []
        for category in x_categories:
            count = categories_like_count_pointer.get(category)
            y_count.append(count)
        plt.figure(figsize=(10, 6))
        plt.bar(x_categories, y_count, linewidth=1.5)
        plt.xlabel("Categories")
        plt.ylabel("Sum of products likes_count")
        plt.title(
            "Sum of products likes_count for each category in ascending order")
        target_path = os.path.join(
            current_dir, "data/figure/generate_likes_count_figure.jpg")
        plt.savefig(target_path)

    # show the relation betwen likes_count and discount in scatter chart

    def generate_discount_likes_count_figure(self):
        current_dir = os.path.dirname(__file__)
        products_path = os.path.join(current_dir, "data/products.txt")
        lines = None
        # this dictionary will store for each dicounts how many like counts
        discount_like_count_pointer = {}
        with open(products_path) as f:
            lines = f.readlines()
        for line in lines:
            converted_line_to_dict = eval(line)
            pro_discount_in_line = int(
                converted_line_to_dict.get('pro_discount'))
            pro_likes_count_in_line = int(
                converted_line_to_dict.get('pro_likes_count'))
            if pro_discount_in_line in discount_like_count_pointer:
                current_value = discount_like_count_pointer.get(
                    pro_discount_in_line)
                discount_like_count_pointer[pro_discount_in_line] = current_value + \
                    pro_likes_count_in_line
            else:
                discount_like_count_pointer[pro_discount_in_line] = pro_likes_count_in_line
        # create list for x-axis and y-axis from the dictionary
        x_discounts = list(discount_like_count_pointer.keys())
        x_discounts.sort()
        y_likes_count = []
        for one_discount in x_discounts:
            count = discount_like_count_pointer.get(one_discount)
            y_likes_count.append(count)
        # draw chart
        plt.figure(figsize=(15, 6))
        plt.scatter(x_discounts, y_likes_count, linewidth=1.5)
        plt.xlabel("X - discount")
        plt.ylabel("Y - likes_count")
        plt.title("Discount Likes Count Figure")
        target_path = os.path.join(
            current_dir, "data/figure/generate_discount_likes_count_figure.jpg")
        plt.savefig(target_path)

    def delete_all_products(self):
        current_dir = os.path.dirname(__file__)
        products_path = os.path.join(current_dir, "data/products.txt")
        open(products_path, 'w').close()
