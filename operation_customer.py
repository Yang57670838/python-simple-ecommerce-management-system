import os
from operation_user import UserOperation
from model_customer import Customer
import re
import math
import time


class CustomerOperation(UserOperation):

    # 2.7.1. requirement just says consists of four parts, for each parts how to validate, I just do as per my work experience
    # which following industry standard for email validation
    # assume input will always be string
    def validate_email(self, user_email):
        return bool(re.search(r"^[\w][\w!#$%&'*+\-\/=?^`.{|}~]+@[\da-zA-Z-]+\.([a-zA-Z]+)$", user_email))

    # 2.7.2. should be exactly 10 digits long, consist only numbers, start with either 04 or 03
    # assume input will always be string
    def validate_mobile(self, user_mobile):
        return bool(re.search(r'^(04|03)\d{8}$', user_mobile))

    def register_customer(self, user_name, user_password, user_email, user_mobile):
        # get database file path
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        # validate first
        if not super().validate_username(user_name):
            return False
        if not super().validate_password(user_password):
            return False
        if not self.validate_email(user_email):
            return False
        if not self.validate_mobile(user_mobile):
            return False
        # check user_name existing in database already or not
        if super().check_username_exist(user_name):
            return False
        # current datetime in format string
        datetime_tuple = time.localtime()
        register_time = time.strftime('%d-%m-%Y_%H:%M:%S', datetime_tuple)
        # create unique user_id, since its not pass as argument
        user_id = super().generate_unique_user_id()
        # since no user_role as argument in requirement, use 'customer' as default value
        user_role = 'customer'
        # encrypt password
        encrypted_user_password = super().encrypt_password(user_password)
        # create a new customer object
        new_customer = Customer(user_id, user_name, encrypted_user_password,
                                register_time, user_role, user_email, user_mobile)
        # write to file
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        content = new_customer.__str__()
        with open(file_path, 'a', encoding='utf-8') as f:
            f.write(content)
            f.write('\n')
        return True

    def update_profile(self, attribute_name, value, customer_object):
        # validate first
        if attribute_name == 'user_name':
            if not super().validate_username(value):
                return False
        if attribute_name == 'user_password':
            if not super().validate_password(value):
                return False
        if attribute_name == 'user_email':
            if not self.validate_email(value):
                return False
        if attribute_name == 'user_mobile':
            if not self.validate_mobile(value):
                return False
        # if attribute_name is not valid
        if attribute_name != 'user_name' and attribute_name != 'user_password' and attribute_name != 'user_email' and attribute_name != 'user_mobile':
            return False
        # locate record index in txt file by unique user_id
        user_id = customer_object.user_id
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        updating_record_index = None
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        for index, line in enumerate(lines):
            # because in every 5 lines, first line contains user_id
            if index % 5 == 0:
                # because user_id has fixed length, we can just grab the slice of string contains user_id value only
                user_id_in_line = line[12:24]
                if user_id_in_line == user_id:
                    updating_record_index = index
                    break
        # in case the user object we try to update is not in database, which should not happen
        if updating_record_index is None:
            return False
        else:
            result = None
            # cache existing record into a new object
            result_user_id = lines[updating_record_index][12:24]
            result_user_name = lines[updating_record_index][40:-3]
            result_user_password = lines[updating_record_index + 1][17:-47]
            result_user_register_time = lines[updating_record_index + 1][-22:-3]
            result_user_role = lines[updating_record_index + 2][13:-3]
            # its for updating customer only, so will always have email and mobile
            result_user_email = lines[updating_record_index + 3][14:-3]
            result_user_mobile = lines[updating_record_index + 4][15:-3]
            # update attribute on the new object
            if attribute_name == 'user_name':
                result_user_name = value
            if attribute_name == 'user_password':
                result_user_password = super().encrypt_password(value)
            if attribute_name == 'user_email':
                result_user_email = value
            if attribute_name == 'user_mobile':
                result_user_mobile = value
            result = Customer(result_user_id, result_user_name, result_user_password, result_user_register_time,
                              result_user_role, result_user_email, result_user_mobile)
            # remove existing record from database txt file
            self.delete_customer(result_user_id)
            # apeend updated record into database with new informations
            content = result.__str__()
            with open(file_path, 'a', encoding='utf-8') as f:
                f.write(content)
                f.write('\n')
            return True

    def delete_customer(self, customer_id):
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        deleting_record_index = None
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        for index, line in enumerate(lines):
            # because in every 5 lines, first line contains user_id
            if index % 5 == 0:
                # because user_id has fixed length, we can just grab the slice of string contains user_id value only
                user_id_in_line = line[12:24]
                if user_id_in_line == customer_id:
                    deleting_record_index = index
                    break
        if deleting_record_index is None:
            return False
        else:
            del lines[deleting_record_index + 4]
            del lines[deleting_record_index + 3]
            del lines[deleting_record_index + 2]
            del lines[deleting_record_index + 1]
            del lines[deleting_record_index]
            # Write original lines back to file again, but skip the deleting record's 5 lines
            with open(file_path, 'w', encoding='utf-8') as f:
                f.writelines(lines)
        return True

    # since requirement does not mention, will assume string as page_number argument type
    def get_customer_list(self, page_number):
        items_per_page = 10
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        lines = None
        all_customer_start_index_list = []
        with open(file_path) as f:
            lines = f.readlines()
        # filter out admin record from list first
        for index, line in enumerate(lines):
            # because in every 5 lines, third line will contains user_role
            if (index - 2) % 5 == 0:
                if line[13:-3] == 'customer':
                    # store all first line index of all customer record
                    all_customer_start_index_list.append(index - 2)
        total_page = math.ceil(len(all_customer_start_index_list) / 10)
        # just in case argument page number is too big for the record available pages
        if int(page_number) > total_page:
            return ([], int(page_number), total_page)
        else:
            # calculate first item in all_customer_start_index_list, as per pagination offset
            first_item_index = (int(page_number) - 1) * items_per_page
            result_customer_object_list = []
            for item_index, location in enumerate(all_customer_start_index_list):
                # get all_customer_start_index for single page
                if item_index >= first_item_index and item_index < first_item_index + 10:
                    # create one customer object from it
                    create_user_id = lines[location][12:24]
                    create_user_name = lines[location][40:-3]
                    create_user_password = lines[location + 1][17:-47]
                    create_user_register_time = lines[location + 1][-22:-3]
                    create_user_email = lines[location + 3][14:-3]
                    create_user_mobile = lines[location + 4][15:-3]
                    create_customer = Customer(create_user_id, create_user_name, create_user_password,
                                               create_user_register_time, 'customer', create_user_email, create_user_mobile)
                    result_customer_object_list.append(create_customer)
            return (result_customer_object_list, int(page_number), total_page)

    def delete_all_customers(self):
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        lines = None
        all_customer_start_index_list = []
        with open(file_path) as f:
            lines = f.readlines()
        # filter out admin record from list first
        for index, line in enumerate(lines):
            # because in every 5 lines, third line will contains user_role
            if (index - 2) % 5 == 0:
                if line[13:-3] == 'customer':
                    # store all first line index of all customer record
                    all_customer_start_index_list.append(index - 2)
        for start_index in all_customer_start_index_list:
            current_line_user_id = lines[start_index][12:24]
            # delete one record from database txt file
            self.delete_customer(current_line_user_id)
