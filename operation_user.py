import random
import string
import re
import os
from model_admin import Admin
from model_customer import Customer


class UserOperation:

    def generate_unique_user_id(self):
        # format: u_10 digits, such as u_1234567890, u_0000000001
        # since total lenght is fixed, so use zfill() to add leading zero if randomly picked number is less than 10 digits
        value = str(random.randint(0, 9999999999)).zfill(10)
        return f'u_{value}'

    def encrypt_password(self, user_password):
        random_string_len = 2 * len(user_password)
        all_letters = string.ascii_letters
        all_digits = string.digits
        # a-zA-Z0-9
        all_letters_digits = all_letters + all_digits
        random_choice = random.sample(all_letters_digits, k=random_string_len)
        random_string = ''.join(random_choice)
        result = '^^'
        for i in range(len(user_password)):
            start_index_for_random_string = i * 2
            result = result + random_string[start_index_for_random_string] + \
                random_string[start_index_for_random_string +
                              1] + user_password[i]
        return result + '$$'

    def decrypt_password(self, encrypted_password):
        return encrypted_password[4:-2:3]

    # 2.6.4
    # since requirement doesnt mention, will just check against all user names for both admin and customer
    def check_username_exist(self, user_name):
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        with open(file_path) as f:
            lines = f.readlines()
            # because every 5 lines hold one user informations for both admin and customer
            picked_lines = lines[::5]
            is_username_exist = False
            for line in picked_lines:
                # because user_id has fixed length, we can just grab the slice of string contains user_name value only
                cutted_line = line[40:-3]
                if (cutted_line == user_name):
                    is_username_exist = True
                    break
            return is_username_exist

    # 2.6.5 The name should only contain letters or underscores, and its length should be at least 5 characters. return True/False
    # assume input will always be string
    def validate_username(self, user_name):
        return bool(re.search(r'^[a-zA-Z_]{5,}$', user_name))

    # The password should contain at least one letter (this letter can be either uppercase or lowercase) and one number.
    # The length of the password must be greater than or equal to 5 characters
    # assume input will always be string
    def validate_password(self, user_password):
        return bool(re.search(r'[a-zA-Z]+', user_password)) and bool(re.search(r'[\d]+', user_password)) and len(user_password) >= 5

    def login(self, user_name, user_password):
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        with open(file_path) as f:
            lines = f.readlines()
            result_index_in_file = None
            for index, line in enumerate(lines):
                # because in every 5 lines, first line contains username
                if index % 5 == 0:
                    # because user_id has fixed length, we can just grab the slice of string contains user_name value only
                    user_name_in_line = line[40:-3]
                    if user_name_in_line == user_name:
                        result_index_in_file = index
                        break
            if result_index_in_file is None:
                # user name not found in database
                return None
            else:
                found_password_line = lines[result_index_in_file + 1]
                # because user_register_time has fixed length
                found_password = found_password_line[17:-47]
                decrypted_found_password = self.decrypt_password(
                    found_password)
                if decrypted_found_password != user_password:
                    # password input not match in database
                    return None
                else:
                    # generate login success object
                    result_password = lines[result_index_in_file + 1][17:-47]
                    result_user_register_time = lines[result_index_in_file + 1][-22:-3]
                    result_user_role = lines[result_index_in_file + 2][13:-3]
                    result_user_id = lines[result_index_in_file][12:24]
                    if result_user_role == 'admin':
                        result = Admin(result_user_id, user_name, result_password,
                                       result_user_register_time, result_user_role)
                    else:
                        result_user_email = lines[result_index_in_file + 3][14:-3]
                        result_user_mobile = lines[result_index_in_file + 4][15:-3]
                        result = Customer(result_user_id, user_name, result_password, result_user_register_time,
                                          result_user_role, result_user_email, result_user_mobile)
                    return result
