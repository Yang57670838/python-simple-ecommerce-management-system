class IOInterface:
    # assume num_of_args is string integer
    def get_user_input(self, message, num_of_args):
        # ask user input
        user_input = input(f'{message}\n')
        # trim leading and trailing white space first
        value = user_input.strip()
        # split white space if there are more inputs
        args = value.split()
        # fullfill arguments if its less than num_of_args
        if len(args) < int(num_of_args):
            gap_num = int(num_of_args) - len(args)
            for _ in range(gap_num):
                args.append('')
        # make return result
        result = []
        for index in range(int(num_of_args)):
            result.append(args[index])
        return result

    # register is for customer only, admin can not register
    def main_menu(self):
        login_menu_options = ['Login', 'Register', 'Quit']
        self.print_message(
            'Welcome to the system, you have the following options:')
        # print main menu options by a loop
        for index, option in enumerate(login_menu_options):
            self.print_message(f'({index + 1}) {option}')

    def admin_menu(self):
        admin_menu_options = ['Show products', 'Add customers', 'Show customers', 'Show orders',
                              'Generate test data', 'Generate all statistical figures', 'Delete all data', 'Logout']
        self.print_message('Hello admin, select below:')
        # print menu options by a loop
        for index, option in enumerate(admin_menu_options):
            self.print_message(f'({index + 1}). {option}')

    def customer_menu(self):
        customer_menu_options = ['Show profile', 'Update profile', 'Show products',
                                 'Show history orders', 'Generate all consumption figures', 'Logout']
        self.print_message('Hello there, please select below:')
        # print menu options by a loop
        for index, option in enumerate(customer_menu_options):
            self.print_message(f'({index + 1}). {option}')

    # format of object_list is for example: ([Order(), Order(), Order()...], page_number, total_page).
    # three types of lists: Customer, Product and Order

    def show_list(self, user_role, list_type, object_list):
        # user_role == 'customer' not allow to print Customer list type
        has_error = False
        if user_role == 'customer':
            if list_type == 'Customer':
                self.print_message('Error: Not allowed')
                has_error = True
        if not has_error:
            instances = object_list[0]
            page_number = object_list[1]
            total_page = object_list[2]
            for index, one_instance in enumerate(instances):
                self.print_message(f'Row {index + 1} record:')
                self.print_object(one_instance)
            self.print_message(f'--------------------------- {page_number} page of {total_page} pages')

    # for example: print_error_message('UserOperation.login', 'username or password incorrect')
    def print_error_message(self, error_source, error_message):
        self.print_message('**********')
        self.print_message(f'Error: {error_message}')
        args = error_source.split('.')
        resouce_class = args[0]
        resource_method = args[1]
        self.print_message(
            f'Error happens from class: {resouce_class}, method: {resource_method}')
        self.print_message('**********')

    def print_message(self, message):
        print(message)

    def print_object(self, target_object):
        print(target_object)
