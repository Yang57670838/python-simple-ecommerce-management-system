import random
import os
import string
import time
from model_admin import Admin
from operation_user import UserOperation


class AdminOperation(UserOperation):
    
    # This function will be called every time run the system. 
    def register_admin(self):
        # get file path first
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        # current datetime in format string
        datetime_tuple = time.localtime()
        register_time = time.strftime('%d-%m-%Y_%H:%M:%S', datetime_tuple)
        # create unique user_id, since its not pass as argument
        user_id = super().generate_unique_user_id()
        # since no user_role as argument in requirement, use 'customer' as default value
        user_role = 'admin'
        # encrypt password
        # since there is no argument, by default, set password for admin as 'password1'
        encrypted_user_password = super().encrypt_password('password1')
        # The same admin account should not be registered multiple times, check with database record
        is_existing_id_in_database = True
        random_admin_user_name = ''
        while is_existing_id_in_database:
            # since there is no argument,, set admin user_name as 'admin_' + randomly 5 letters
            all_letters = string.ascii_letters
            random_choice = random.sample(all_letters, k=5)
            random_string = ''.join(random_choice) 
            random_admin_user_name = f'admin_{random_string}'
            is_existing_id_in_database = super().check_username_exist(random_admin_user_name)
        # create a new admin object
        new_admin = Admin(user_id, random_admin_user_name, encrypted_user_password, register_time, user_role)
        # write to file
        # add two new empty lines afterwards, because want to make sure every record no matter admin or custmer will cost 5 lines in the txt file
        # just for simplify logic purpose, as one of many ways.. since no requirements limit this..
        content = f'{new_admin.__str__()}\nNA\nNA'
        with open(file_path, 'a', encoding='utf-8') as f:
            f.write(content)
            f.write('\n')