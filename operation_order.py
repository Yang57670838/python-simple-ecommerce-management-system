import random
import os
import string
import time
import math
from model_order import Order
from operation_customer import CustomerOperation
from operation_product import ProductOperation
import matplotlib.pyplot as plt


class OrderOperation:
    # format is o_5 digits
    def generate_unique_order_id(self):
        # as requirement, we need to check if there is existing id or not in the orders.txt file
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/orders.txt")
        is_existing_id_in_database = True
        new_id = ''
        while is_existing_id_in_database:
            is_existing_id_in_database = False
            # format: o_ 5 digits, such as o_12345
            # since total lenght is fixed, so use zfill() to add leading zero if randomly picked number is less than 5 digits
            value = str(random.randint(0, 99999)).zfill(5)
            new_id = f'o_{value}'
            # scan txt file and check if existing
            lines = None
            with open(file_path) as f:
                lines = f.readlines()
            for index, line in enumerate(lines):
                # because in every 4 lines, first line will contains order_id
                if index % 4 == 0:
                    if line[:7] == new_id:
                        is_existing_id_in_database = True
        return new_id

    def create_an_order(self, customer_id, product_id, create_time=None):
        # since requirement ask return True/False, so wrap with try block, when catch exception will return False
        # but logically its not good idea, what if we face exception when already write some lines in txt file, there is no rollback logic
        # its bit over engineer for this requirement.. so just simply wrap with try block
        try:
            # create_time, use the current time if not provided, so create_time is an optional argument
            if create_time is None:
                datetime_tuple = time.localtime()
                create_time = time.strftime(
                    '%d-%m-%Y_%H:%M:%S', datetime_tuple)
            order_id = self.generate_unique_order_id()
            new_order = Order(order_id, customer_id, product_id, create_time)
            # write to file
            current_dir = os.path.dirname(__file__)
            order_file_path = os.path.join(current_dir, "data/orders.txt")
            with open(order_file_path, 'a', encoding='utf-8') as f:
                # since requirement doesnt mention, will write as four lines equal to order_id, user_id, pro_id, order_time
                # this format can make search easier later
                f.write(new_order.order_id)
                f.write('\n')
                f.write(new_order.user_id)
                f.write('\n')
                f.write(new_order.pro_id)
                f.write('\n')
                f.write(new_order.order_time)
                f.write('\n')
        except:
            return False
        else:
            return True

    def delete_order(self, order_id):
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/orders.txt")
        deleting_record_index = None
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        for index, line in enumerate(lines):
            # because in every 4 lines, first line be order_id
            if index % 4 == 0:
                # because each line also contains a \n in the content
                order_id_in_line = line[0:7]
                if order_id_in_line == order_id:
                    deleting_record_index = index
                    break
        if deleting_record_index is None:
            return False
        else:
            del lines[deleting_record_index + 3]
            del lines[deleting_record_index + 2]
            del lines[deleting_record_index + 1]
            del lines[deleting_record_index]
            # Write original lines back to file again, but skip the deleting record's 4 lines
            with open(file_path, 'w', encoding='utf-8') as f:
                f.writelines(lines)
        return True

    # since requirement does not mention, will assume string as page_number argument type
    def get_order_list(self, customer_id, page_number):
        items_per_page = 10
        # read all orders and filter by customer_id first
        filter_orders_first_line_index_list = []
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/orders.txt")
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        for index, line in enumerate(lines):
            # because in every 4 lines, second line will contains user_id
            if (index - 1) % 4 == 0:
                if line[:12] == customer_id:
                    # store each record's first line index
                    filter_orders_first_line_index_list.append(index - 1)
        # calculate total pages by round up
        total_page = math.ceil(
            len(filter_orders_first_line_index_list) / items_per_page)
        # just in case argument page number is too big for the record available pages
        if int(page_number) > total_page:
            return ([], int(page_number), total_page)
        else:
            # calculate first item index for the page from the filter_orders_first_line_index_list
            first_item_index = (int(page_number) - 1) * items_per_page
            # slice the big list into smaller list just for single page
            # for last page, may not have maximum 10 products, this slice will still work by just return available items
            orders_first_line_index_list_in_page = filter_orders_first_line_index_list[
                first_item_index: first_item_index + items_per_page]
            # create list of Product instance now
            result_products_list = []
            for one_record_first_line_index in orders_first_line_index_list_in_page:
                # create Order instance from its first line index
                order_id = lines[one_record_first_line_index][:7]
                product_id = lines[one_record_first_line_index + 2][:-1]
                create_time = lines[one_record_first_line_index + 3][:-1]
                new_order = Order(order_id, customer_id,
                                  product_id, create_time)
                result_products_list.append(new_order)
            return (result_products_list, int(page_number), total_page)

    def generate_test_order_data(self):
        # create 10 customers in a loop first
        customer_opeation = CustomerOperation()
        for _ in range(10):
            # because for each one customer we try to create, its possible to fail, for example, create with existing user name
            # so here use a whilte loop to keep try for each time of create
            is_one_time_create_customer_fail = True
            while is_one_time_create_customer_fail:
                # geenrate random customer name
                all_letters = string.ascii_letters
                random_choice = random.sample(all_letters, k=5)
                random_string = ''.join(random_choice)
                random_customer_user_name = f'customer_{random_string}'
                # use password1 as default password for each customer
                default_customer_password = 'password1'
                # generate random customer email
                random_user_email = f'{random_customer_user_name}@gmail.com'
                # generate random customer mobile
                all_digits = string.digits
                random_mobile_choice = random.sample(all_digits, k=8)
                random_mobile_digits = ''.join(random_mobile_choice)
                random_customer_mobile = f'04{random_mobile_digits}'
                register_customer_result = customer_opeation.register_customer(
                    random_customer_user_name, default_customer_password, random_user_email, random_customer_mobile)
                if register_customer_result:
                    is_one_time_create_customer_fail = False
        # because requirement only allows register_customer() can return True/False, so here have to scan the txt file to read last 10 customers ids
        generated_customer_ids = []
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/users.txt")
        with open(file_path) as f:
            lines = f.readlines()
            # because every 5 lines hold one user informations for both admin and customer, but last 10 must be new created customers
            customer_id_lines = lines[::5]
            last_ten_customer_id_lines = customer_id_lines[-10:]
            for one_line in last_ten_customer_id_lines:
                customer_id_in_line = one_line[12:24]
                generated_customer_ids.append(customer_id_in_line)
        # get all product_id from txt file, so later can randomly choose products from it
        products_file_path = os.path.join(current_dir, "data/products.txt")
        products_lines = None
        available_products_ids = []
        with open(products_file_path) as f:
            products_lines = f.readlines()
        for product_line in products_lines:
            # to convert string of dictionary into dictionary, since no other library is allowed, using eval()
            converted_line_to_dict = eval(product_line)
            pro_id_in_line = converted_line_to_dict.get('pro_id')
            available_products_ids.append(pro_id_in_line)
        for one_customer_id in generated_customer_ids:
            # for each customer, randomly generate 50-200 orders
            numer_of_orders = random.randint(50, 200)
            # let the time be scattered into different 12 months of the year. so we will switch month one by one in each order in the loop
            # through month_float_point + 1
            month_float_point = 1
            for _ in range(numer_of_orders):
                # since create_an_order will return True/False, here will use while loop to try to create each order, and only create next order if previous create_an_order return True
                is_create_an_order_fail = True
                while is_create_an_order_fail:
                    current_time = time.localtime()
                    year, month, day = current_time.tm_year, current_time.tm_mon, current_time.tm_mday
                    # this way, it will spread into 12 different months for each order
                    new_month = (month + month_float_point) % 12
                    month_float_point = month_float_point + 1
                    if new_month == 0:
                        new_month = 12
                    modified_time = time.struct_time((year, new_month, day, current_time.tm_hour, current_time.tm_min,
                                                     current_time.tm_sec, current_time.tm_wday, current_time.tm_yday, current_time.tm_isdst))
                    formatted_modified_time = time.strftime(
                        '%d-%m-%Y_%H:%M:%S', modified_time)
                    random_product_id = random.choice(available_products_ids)
                    create_new_order = self.create_an_order(
                        one_customer_id, random_product_id, formatted_modified_time)
                    # stop the while loop since create one order success
                    if create_new_order:
                        is_create_an_order_fail = False

    # this function is a bit slow
    def generate_single_customer_consumption_figure(self, customer_id):
        # filter orders for single customer first
        single_customer_orders_first_line_index_list = []
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/orders.txt")
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        for index, line in enumerate(lines):
            # because in every 4 lines, second line will contains user_id
            if (index - 1) % 4 == 0:
                if line[:12] == customer_id:
                    # store each record's first line index
                    single_customer_orders_first_line_index_list.append(
                        index - 1)
        # create dictionary, to store all 12 months cost
        monthly_cost_count = {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
            7: 0,
            8: 0,
            9: 0,
            10: 0,
            11: 0,
            12: 0,
        }
        production_operation = ProductOperation()
        for one_record_first_line_index in single_customer_orders_first_line_index_list:
            create_time_in_line = lines[one_record_first_line_index + 3][:-1]
            month_in_line = int(create_time_in_line[3:5])
            product_id_in_line = lines[one_record_first_line_index + 2][:-1]
            # get the cost of this order
            found_product = production_operation.get_product_by_id(
                product_id_in_line)
            # in case product id not found
            if found_product is None:
                # no change
                current_cost_for_this_month = monthly_cost_count.get(
                    month_in_line)
                monthly_cost_count[month_in_line] = current_cost_for_this_month
            else:
                # sum up
                current_cost_for_this_month = monthly_cost_count.get(
                    month_in_line)
                # round up with 2 decimals
                new_cost = current_cost_for_this_month + \
                    float(found_product.pro_current_price)
                monthly_cost_count[month_in_line] = round(new_cost, 2)
        # draw bar chart
        x_months = list(monthly_cost_count.keys())
        y_count = []
        for month in x_months:
            count = monthly_cost_count.get(month)
            y_count.append(count)
        plt.figure(figsize=(15, 6))
        plt.bar(x_months, y_count, linewidth=1.5)
        plt.xlabel("12 months")
        plt.ylabel("Sum of order price")
        plt.title(
            "Sum of order price for 12 different months for the given customer")
        target_path = os.path.join(
            current_dir, "data/figure/generate_single_customer_consumption_figure.jpg")
        plt.savefig(target_path)

    # this function will be super slow

    def generate_all_customers_consumption_figure(self):
        production_operation = ProductOperation()
        # create dictionary, to store all 12 months cost
        monthly_cost_count = {
            '01': 0,
            '02': 0,
            '03': 0,
            '04': 0,
            '05': 0,
            '06': 0,
            '07': 0,
            '08': 0,
            '09': 0,
            '10': 0,
            '11': 0,
            '12': 0,
        }
        # scan orders
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/orders.txt")
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        for index, line in enumerate(lines):
            # because in every 4 lines will contains one order as per current design
            if index % 4 == 0:
                # calculate month and price for this order
                create_time_in_line = lines[index + 3][:-1]
                month_in_line = create_time_in_line[3:5]
                product_id_in_line = lines[index + 2][:-1]
                # get the cost of this order
                found_product = production_operation.get_product_by_id(
                    product_id_in_line)
                # in case product id not found
                if found_product is None:
                    # no change
                    current_cost_for_this_month = monthly_cost_count.get(
                        month_in_line)
                    monthly_cost_count[month_in_line] = current_cost_for_this_month
                else:
                    # sum up
                    current_cost_for_this_month = monthly_cost_count.get(
                        month_in_line)
                    # round up with 2 decimals
                    new_cost = current_cost_for_this_month + \
                        float(found_product.pro_current_price)
                    monthly_cost_count[month_in_line] = round(new_cost, 2)
        # draw bar chart
        x_months = list(monthly_cost_count.keys())
        y_count = []
        for month in x_months:
            count = monthly_cost_count.get(month)
            y_count.append(count)
        plt.figure(figsize=(15, 6))
        plt.bar(x_months, y_count, linewidth=1.5)
        plt.xlabel("12 months")
        plt.ylabel("Sum of order price")
        plt.title(
            "Sum of order price for 12 different months for all customers")
        target_path = os.path.join(
            current_dir, "data/figure/generate_all_customers_consumption_figure.jpg")
        plt.savefig(target_path)

    # a little bit slow
    def generate_all_top_10_best_sellers_figure(self):
        production_operation = ProductOperation()
        # count selling result for each product_id for all orders
        # create a dictionary to store the courting result
        all_products_selling_count = {}
        current_dir = os.path.dirname(__file__)
        file_path = os.path.join(current_dir, "data/orders.txt")
        lines = None
        with open(file_path) as f:
            lines = f.readlines()
        for index, line in enumerate(lines):
            # because in every 4 lines, third line will contains product id
            if (index - 2) % 4 == 0:
                pro_id_in_line = line[:-1]
                if pro_id_in_line in all_products_selling_count:
                    current_value = all_products_selling_count.get(
                        pro_id_in_line)
                    all_products_selling_count[pro_id_in_line] = current_value + 1
                else:
                    all_products_selling_count[pro_id_in_line] = 1
        sorted_all_products_selling_count = sorted(
            all_products_selling_count.items(), key=lambda x: x[1])
        top_10_products_id_list = sorted_all_products_selling_count[-10:]
        # get each product name by its id and store in a dictionary
        result_store = {}
        for item in top_10_products_id_list:
            product_id = item[0]
            selling_counts = item[1]
            # get the cost of this order
            found_product = production_operation.get_product_by_id(product_id)
            pro_name = found_product.pro_name
            result_store[pro_name] = selling_counts
        # draw bar chart
        x_products = list(result_store.keys())
        # sort the result in descending order.
        x_products.sort(reverse=True)
        y_count = []
        for product in x_products:
            count = result_store.get(product)
            y_count.append(count)
        plt.figure(figsize=(20, 6))
        plt.bar(x_products, y_count, linewidth=1.5)
        # product name too long, have to rotate it
        plt.xticks(rotation=45, ha='right', fontsize=10)
        plt.xlabel("10 Products")
        plt.ylabel("selling counts")
        plt.title(
            "Top 10 best selling products")
        plt.tight_layout()
        target_path = os.path.join(
            current_dir, "data/figure/generate_all_top_10_best_sellers_figure.jpg")
        plt.savefig(target_path)

    def delete_all_orders(self):
        # self.delete_order(id) will need to rewrite all file every time delete one order, it not makes sense to use it in a loop
        # instead, just use below way to clean the order txt file
        current_dir = os.path.dirname(__file__)
        order_path = os.path.join(current_dir, "data/orders.txt")
        open(order_path, 'w').close()
